﻿
using Practice.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Practice.Service
{
    public static class TableMaker
    {
    	public static string MakeTable(List<User> users,int tableWidth)
        {
            var builder = new StringBuilder();

            builder.AppendLine(MakeLine('*', tableWidth));
            builder.AppendLine(MakeRow('*', tableWidth, "Name", "Age", "Gender", "Id"));
            builder.AppendLine(MakeLine('*', tableWidth));
            foreach (var user in users)
            {
                builder.AppendLine(MakeRow('*', tableWidth, user.Name, user.Age.ToString(), user.Gender, user.Id.ToString()));
                builder.AppendLine(MakeLine('*', tableWidth));
            }

            return builder.ToString();
        }

        public static string MakeLine(char fence, int tableWidth)
        {
            return new string(fence, tableWidth);
        }

        public static string MakeRow(char fence, int tableWidth, params string[] variables)
        {
            int width = (tableWidth - variables.Length) / variables.Length;
            var stringBuilder = new StringBuilder();
            stringBuilder.Append(fence);

            foreach (string column in variables)
            {
                stringBuilder.Append(AlignCentre(column, width) + fence);
            }

            return stringBuilder.ToString();
        }
        static string AlignCentre(string text, int width)
        {
            text = text.Length > width ? text.Substring(0, width - 3) + "..." : text;

            if (string.IsNullOrEmpty(text))
            {
                return new string(' ', width);
            }
            else
            {
                return text.PadRight(width - (width - text.Length) / 2).PadLeft(width);
            }
        }
    }
}
