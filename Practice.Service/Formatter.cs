﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Channels;
using Practice.Models;

namespace Practice.Service
{
    public class Formatter
    {
        public string ToFormat(List<User> users)
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.AppendLine("{");

            foreach (var user in users)
            {
                stringBuilder.Append($"[ Name:{user.Name};");
                stringBuilder.Append($"Age:{user.Age};");
                stringBuilder.Append($"Id:{user.Id};]");
                stringBuilder.Append($"Gender:{user.Gender};]\n");
            }

            stringBuilder.AppendLine("}");
            return stringBuilder.ToString();
        }

        public User FromFormatToUser(string jasonstatham)
        {
            var values = jasonstatham.Split('\n').Where(s => s.StartsWith('[') && s.EndsWith(']')).ToList();
            var value = values[0];
            Console.WriteLine(value);
            int start, end;
            var user = new User();
            try
            {
                string str = value.Substring(value.IndexOf("Name:"));
                start = str.IndexOf(':') + 1;
                end = str.IndexOf(';');
                user.Name = str.Substring(start, end - start);

                str = value.Substring(value.IndexOf("Gender:"));
                start = str.IndexOf(':') + 1;
                end = str.IndexOf(';');
                user.Gender = str.Substring(start, end - start);

                str = value.Substring(value.IndexOf("Age:"));
                start = str.IndexOf(':') + 1;
                end = str.IndexOf(';');
                user.Age = int.Parse(str.Substring(start, end - start));

                str = value.Substring(value.IndexOf("Id:"));
                start = str.IndexOf(':') + 1;
                end = str.IndexOf(';');
                user.Id = int.Parse(str.Substring(start, end - start));
            }
            catch (Exception exception)
            {
                Console.WriteLine($"SOMETHING WRONG WITH FORMATTER\n{exception.Message}");
            }
            return user;
        }
        public List<User> FromFormatToUsers(string jasonstatham)
        {
            var result = new List<User>();

            //хуже уже некуда бля
            var values = jasonstatham.Split('\n').Where(s => s.StartsWith('[') && s.EndsWith(']')).ToList();
            int start;
            int end;
            for (int i = 0; i < values.Count; i++)
            {
                var user = new User();
                try
                {
                    string str = values[i].Substring(values[i].IndexOf("Name:"));
                    start = str.IndexOf(':') + 1;
                    end = str.IndexOf(';');
                    user.Name = str.Substring(start, end - start);

                    str = values[i].Substring(values[i].IndexOf("Gender:"));
                    start = str.IndexOf(':') + 1;
                    end = str.IndexOf(';');
                    user.Gender = str.Substring(start, end - start);

                    str = values[i].Substring(values[i].IndexOf("Age:"));
                    start = str.IndexOf(':') + 1;
                    end = str.IndexOf(';');
                    user.Age = int.Parse(str.Substring(start, end - start));

                    str = values[i].Substring(values[i].IndexOf("Id:"));
                    start = str.IndexOf(':') + 1;
                    end = str.IndexOf(';');
                    user.Id = int.Parse(str.Substring(start, end - start));

                    result.Add(user);
                }

                catch (Exception exception)
                {
                    Console.WriteLine($"SOMETHING WRONG WITH FORMATTER\n{exception.Message}");
                }

            }

            return result;
        }
    }
}
