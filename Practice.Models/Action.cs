﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Practice.Models
{
    public static class Action
    {
        public static string Get { get; } = "/users/get";
        public static string Update { get; } = "/users/update";
        public static string Remove { get; } = "/users/remove";
        public static string Add { get; } = "/users/add";
    }
}
