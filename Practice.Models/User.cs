﻿using System;

namespace Practice.Models
{
    [Serializable]
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; } = "N/A";
        public string Gender { get; set; } = "N/A";
        public int Age { get; set; } = -1;
    }
}
