﻿using System;

namespace Practice.Models
{
    [Serializable]
    public class Request
    {
        public string Action { get; set; }
        public string Data { get; set; }
    }
}
