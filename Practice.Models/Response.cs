﻿using System;

namespace Practice.Models
{
    [Serializable]
    public class Response
    {
        public int Code { get; set; }
        public string To { get; set; }
        public string Data { get; set; }
    }
}
