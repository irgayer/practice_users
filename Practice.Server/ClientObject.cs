﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Practice.Models;
using Practice.Service;
using Action = Practice.Models.Action;

namespace Practice.Server
{
    public class ClientObject
    {
        protected internal string Id { get; private set; }
        protected internal NetworkStream Stream { get; private set; }
        TcpClient client;
        ServerObject server;
        Formatter formatter = new Formatter();
        ByteConverter byteConverter = new ByteConverter();
        DbManager dbManager = new DbManager();

        public ClientObject(TcpClient tcpClient, ServerObject serverObject)
        {
            Id = Guid.NewGuid().ToString();
            client = tcpClient;
            server = serverObject;
            serverObject.AddConnection(this);
        }

        public async void Process()
        {
            try
            {
                Stream = client.GetStream();

                while (true)
                {
                    try
                    {
                        Request request = await GetRequestAsync();
                        Console.WriteLine($"{request.Action}  { ((IPEndPoint)client.Client.RemoteEndPoint).Address.ToString()}");
                        
                           
                        var response = new Response();
                        response.To = this.Id;
                        if (request != null)
                        {
                            if (request.Action == Action.Get)
                            {
                                var users = await dbManager.GetAllUsers();
                                if (users.Count == 0)
                                    response.Code = 202;

                                else response.Code = 200;
                                response.Data = formatter.ToFormat(users);
                            }
                            else if (request.Action == Action.Add)
                            {
                                var users = formatter.FromFormatToUsers(request.Data);
                                await dbManager.AddUsers(users);
                                response.Code = 201;
                                response.Data = null;
                            }
                            //параша
                            else if (request.Action == Action.Remove)
                            {
                                uint.TryParse(request.Data, out uint id);
                                bool success = await dbManager.RemoveUser((int)id);
                                if (success != true)
                                    response.Code = 400;
                                else
                                    response.Code = 201;
                                response.Data = null;
                            }
                            else if (request.Action == Action.Update)
                            {
                                var users = formatter.FromFormatToUsers(request.Data);
                                bool success = await dbManager.UpdateUser(users[0]);
                                if (success != true)
                                    response.Code = 400;
                                else
                                    response.Code = 201;
                                response.Data = null;
                            }
                            else
                            {
                                response.Code = 400;
                                response.Data = null;
                            }

                            await Task.Delay(5000);
                            await server.SendResponse(response);
                        }
                        else
                        {
                            Console.WriteLine("Пришел пустой запрос!");
                        }
                    }
                    catch
                    {
                        //Console.WriteLine();
                    }
                }
            }

            catch (Exception exception)
            {
                Console.WriteLine("Ошибка прослушивания клиента!");
                Console.WriteLine(exception.Message);
            }
            finally
            {
                server.RemoveConnection(this.Id);
            }
        }

        private async Task<Request> GetRequestAsync()
        {
            byte[] data = new byte[1024];
            StringBuilder builder = new StringBuilder();
            Request request = null;
            int bytes = 0;
            do
            {
                bytes = await Stream.ReadAsync(data, 0, data.Length);
                request = (Request)byteConverter.ByteArrayToObject(data);
            } while (Stream.DataAvailable);

            return request;
        }
        protected internal void Close()
        {
            Stream?.Close();
            client?.Close();
        }
    }
}
