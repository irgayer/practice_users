﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Practice.Models;

namespace Practice.Server
{
    public class DbManager
    {
        public async Task<List<User>> GetAllUsers()
        {
            using (var context = new AppContext())
            {
                return await context.Users.ToListAsync();
            }
        }

        public async Task AddUsers(List<User> users)
        {

            foreach (var user in users)
            {
                await AddUser(user);
            }


        }
        public async Task AddUser(User user)
        {
            using (var context = new AppContext())
            {
                await context.Users.AddAsync(user);
                try
                {
                    await context.SaveChangesAsync();
                }
                catch
                {

                }
            }
        }
        public async Task RemoveUsers(List<int> ids)
        {
            foreach (var id in ids)
            {
                await RemoveUser(id);
            }
        }
        //public async Task<bool> UpdateUsers(List<User> users)
        //{
        //    foreach (var user in users)
        //    {
        //        await UpdateUser(user);
        //    }
        //}
        public async Task<bool> RemoveUser(int id)
        {
            bool flag = false;
            using (var context = new AppContext())
            {
                List<User> users = await context.Users.ToListAsync();
                for (int i = 0; i < users.Count; i++)
                {
                    if (id == users[i].Id)
                    {
                        context.Users.Remove(users[i]);

                        try
                        {
                            await context.SaveChangesAsync();
                        }
                        catch
                        {
                            Console.WriteLine("не сохранилось");
                        }
                        return true;
                    }
                }

            }
            return flag;
        }

        public async Task<bool> UpdateUser(User user)
        {
            using (var context = new AppContext())
            {
                List<User> users = await context.Users.ToListAsync();
                for (int i = 0; i < users.Count; i++)
                {
                    if (user.Id == users[i].Id)
                    {
                        users[i].Name = user.Name;
                        users[i].Age = user.Age;
                        users[i].Gender = user.Gender;
                        try
                        {
                            await context.SaveChangesAsync();
                        }
                        catch
                        {
                            Console.WriteLine("не обновилось");
                        }
                        return true;
                    }
                  
                }
                return false;

            }
        }
    }
}
