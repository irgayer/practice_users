﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using Practice.Models;
using Practice.Service;

namespace Practice.Server
{
    class Program
    {
        static async Task Main(string[] args)
        {
     
            DbManager d = new DbManager();

            var server = new ServerObject();

            try
            {
                Task task = Task.Run(()=>server.ListenAsync("10.1.4.72",3231));
            }
            catch (Exception ex)
            {
                server.Disconnect();
                Console.WriteLine(ex.Message);
            }

            Console.ReadLine();
        }



    }
}
