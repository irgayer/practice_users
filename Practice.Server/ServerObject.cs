﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Practice.Models;
using Practice.Service;

namespace Practice.Server
{
    public class ServerObject
    {
        static TcpListener tcpListener;
        List<ClientObject> clients = new List<ClientObject>();
        ByteConverter byteConverter = new ByteConverter();

        protected internal void AddConnection(ClientObject clientObject)
        {
            clients.Add(clientObject);
        }
        protected internal void RemoveConnection(string id)
        {
            var client = clients.FirstOrDefault(c => c.Id == id);
            if (client != null)
                clients.Remove(client);
        }
        public async Task ListenAsync(string ip, int port)
        {
            try
            {
                tcpListener = new TcpListener(IPAddress.Parse(ip), port);
                tcpListener.Start();
                Console.WriteLine("Сервер запущен. Ожидание подключений...");
                while (true)
                {
                    TcpClient tcpClient = await tcpListener.AcceptTcpClientAsync();
                    var clientObject = new ClientObject(tcpClient, this);

                    Console.WriteLine($"{((IPEndPoint)tcpClient.Client.RemoteEndPoint).Address.ToString()} подключился.");
                    clientObject.Process();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Disconnect();
            }
        }
        protected internal async Task SendResponse(Response response)
        {
            byte[] data = byteConverter.ObjectToByteArray(response);
            try
            {
                foreach (var client in clients)
                {
                    if (client.Id == response.To)
                    {
                        await client.Stream.WriteAsync(data, 0, data.Length);
                    }
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
        }

        protected internal void Disconnect()
        {
            tcpListener.Stop(); 

            foreach (var client in clients)
            {
                client.Close();
            }
            Environment.Exit(0); 
        }
    }
}
