﻿using System;
using System.Threading.Tasks;
using Practice.Models;
using Practice.Service;

namespace Practice.Client
{
    class Program
    {
        static async Task Main(string[] args)
        {

           using var app = new ClientApp("10.1.4.72", 3231);
            await app.StartAsync();
            Console.ReadLine();
        }

    }
}