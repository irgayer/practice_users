﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using Practice.Models;
using Practice.Service;
using Action = Practice.Models.Action;

namespace Practice.Client
{
    public class ClientApp : IDisposable
    {
        private readonly IPEndPoint endPoint;
        private readonly TcpClient client;
        private readonly ByteConverter byteConverter;
        private Formatter formatter = new Formatter();
        private NetworkStream stream;

        public ClientApp(string address = "127.0.0.1", int port = 3231)
        {
            endPoint = new IPEndPoint(IPAddress.Parse(address), port);
            client = new TcpClient();
            byteConverter = new ByteConverter();
        }

        private int Menu(params string[] choices)
        {
            foreach (var choice in choices)
            {
                Console.WriteLine(choice);
            }

            if (int.TryParse(Console.ReadLine(), out int result))
            {
                Console.Clear();
                return result;
            }

            return -1;
        }

        public async Task StartAsync()
        {
            try
            {
                client.Connect(endPoint);
                stream = client.GetStream();

                while (true)
                {

                    int menu = Menu("1) Получить пользователей", "2) Добавить пользователя", "3) Обновить пользователя", "4) Удалить пользователя");
                    Request request = new Request();
                    if (menu == 1)
                    {
                        request.Action = Action.Get;
                        await SendRequestAsync(request);


                        await ReceiveResponse();
                    }
                    else if (menu == 2)
                    {
                        Console.WriteLine("Введите гендер:");
                        string userGender = Console.ReadLine();
                        Console.WriteLine("Введите имя");
                        string userName = Console.ReadLine();
                        Console.WriteLine("Введите возраст:");
                        if (uint.TryParse(Console.ReadLine(), out uint userAge))
                        {
                            request.Action = Action.Add;
                            User user = new User() { Age = (int)userAge, Gender = userGender, Name = userName };
                            request.Data = formatter.ToFormat(new List<User>() { user });
                            await SendRequestAsync(request);
                            await ReceiveResponse();
                        }
                        else
                        {
                            Console.WriteLine("Введены некорректные данные!");
                        }

                    }
                    else if (menu == 3)
                    {
                        Console.WriteLine("Введите id пользователя которого вы хотите обновить данные");
                        if (int.TryParse(Console.ReadLine(), out int id))
                        {

                            Console.WriteLine("Введите гендер:");
                            string userGender = Console.ReadLine();
                            Console.WriteLine("Введите имя");
                            string userName = Console.ReadLine();
                            Console.WriteLine("Введите возраст:");
                            if (uint.TryParse(Console.ReadLine(), out uint userAge))
                            {
                                request.Action = Action.Update;
                                User user = new User() { Id = id, Age = (int)userAge, Gender = userGender, Name = userName };
                                request.Data = formatter.ToFormat(new List<User>() { user });
                                await SendRequestAsync(request);
                                await ReceiveResponse();
                            }
                            else
                            {
                                Console.WriteLine("Введены некорректные данные!");
                            }
                        }
                           else
                        {
                            Console.WriteLine("Введены некорректные данные!");
                        }
                    }
                    else if (menu == 4)
                    {
                        Console.WriteLine("Введите id");
                        if (int.TryParse(Console.ReadLine(), out int id))
                        {
                            request.Action = Action.Remove;

                            request.Data = id.ToString();
                            await SendRequestAsync(request);
                            await ReceiveResponse();
                        }
                        else
                        {
                            Console.WriteLine("Введены некорректные данные!");
                        }


                    }



                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Disconnect();
            }
        }
        async Task SendRequestAsync(Request request)
        {
            var data = byteConverter.ObjectToByteArray(request);
            await stream.WriteAsync(data, 0, data.Length);

        }

        async Task ReceiveResponse()
        {

            Console.WriteLine("Ожидание ответа...");
            int bytes = 0;
            try
            {
                var data = new byte[1024];
                do
                {
                    bytes = await stream.ReadAsync(data, 0, data.Length);
                    Response response = (Response)byteConverter.ByteArrayToObject(data);

                    if (response.Code == 200)
                    {
                        var users = formatter.FromFormatToUsers(response.Data);
                        Console.WriteLine(TableMaker.MakeTable(users, 75));
                    }
                    else if (response.Code == 202)
                    {
                        Console.WriteLine("Пользователей нет!");
                    }
                    else if (response.Code == 201)
                    {
                        Console.WriteLine("Успешно!");
                    }
                    else
                    {
                        Console.WriteLine("Что-то пошло не так");
                    }

                }
                while (stream.DataAvailable);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Подключение прервано!");
                Console.ReadLine();
                Disconnect();
            }

        }

        void Disconnect()
        {
            stream?.Close();
            client?.Close();
            Environment.Exit(0);
        }

        public void Dispose()
        {
            Disconnect();
        }
    }
}
